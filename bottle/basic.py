# -*- coding:utf-8 -*-
#再帰させる関数があるため自身を読み込む
import csv
import basic


#array[x][y]にnumを挿入して変更gの配列w返す
#引数はx、y、挿入する配列、挿入するデータ
def add(x,y,array,num):
    tmparray = array
    tmparray[x][y] = num
    return tmparray

#手番を入れ替える関数(もうちょっとスマートにできるかも①)
def skip():
        skipfile = open("predata.txt","r")
        skip = skipfile.read()
        if skip == "1":
            skip = "0"
        else:
            skip = "1"
        skipfile.close()
        skipfile = open("predata.txt","w")
        skipfile.write(skip)
        skipfile.close()

#csv形式のデータを読み込み配列で返す
#引数はcsvファイル
def readfile(txt):
        tmpfile=open(txt,"r")
        tmpcsv_obj=csv.reader(tmpfile)
        tmpdata=[ v for v in tmpcsv_obj]
        tmpfile.close()
	return tmpdata

#配列をファイルにcsv形式で書き込む
#引数は書き込む配列、書き込み先のファイル
def writefile(array,txt):
        tmpfile = open(txt,"w")
        tmpcsv = csv.writer(tmpfile, lineterminator="\n")
        tmpcsv.writerows(array)
        tmpfile.close()
        
#配列を引数に従って書き換えて、書き換え後の配列を返す関数（もうちょっとうまく書けるかも②）
#switch的な関数で実装したほうがちょっときれい？
#引数は書き換える配列、x、y、上0 下1 左2 右3 右上4 左上5 右下6 左下7、書き換える要素数、書き換える数字
def reWrite(array,x,y,direction,num,new):
	tmparray=array
	if direction == 0:
		for i in range(1,1+num):
			tmparray[x-i][y]=new

        if direction == 1:
                for i in range(1,1+num):
                        tmparray[x+i][y]=new

        if direction == 2:
                for i in range(1,1+num):
                        tmparray[x][y-i]=new

        if direction == 3:
                for i in range(1,1+num):
                        tmparray[x][y+i]=new

        if direction == 4:
                for i in range(1,1+num):
                        tmparray[x-i][y+i]=new

        if direction == 5:
                for i in range(1,1+num):
                        tmparray[x-i][y-i]=new

        if direction == 6:
                for i in range(1,1+num):
                        tmparray[x+i][y+i]=new

        if direction == 7:
                for i in range(1,1+num):
                        tmparray[x+i][y-i]=new

	return tmparray


#オセロみたいに挟まれたら反転させ、反転後の配列を返す関数（もうちょっとうまく書けるかも③）
#引数　書き換える配列、x、y、繰り返し回数、上0 下1 左2 右3 右上4 左上5 右下6 左下7、挿入される数字
def samej(array,x,y,time,direction,num):
	tmparray=array
	#繰り返し回数が0の時（初回）はすべての方向を読み込み、8方向について反転するかの確認と反転するのであれば反転させる
	if time == 0:
		tmparray = basic.samej(tmparray,x,y,time+1,0,num)
                tmparray = basic.samej(tmparray,x,y,time+1,1,num)
                tmparray = basic.samej(tmparray,x,y,time+1,2,num)
                tmparray = basic.samej(tmparray,x,y,time+1,3,num)
                tmparray = basic.samej(tmparray,x,y,time+1,4,num)
                tmparray = basic.samej(tmparray,x,y,time+1,5,num)
                tmparray = basic.samej(tmparray,x,y,time+1,6,num)
                tmparray = basic.samej(tmparray,x,y,time+1,7,num)
	#盤面の外に出たら確認終了
	elif(direction==0 and x-time < 0) or (direction==1 and x+time >7) or (direction==2 and y-time < 0) or (direction==3 and y+time > 7):
                return tmparray
        elif direction==4 and (x-time < 0 or y+time > 7):
                return tmparray
        elif direction==5 and (x-time < 0 or y-time < 0):
                return tmparray
        elif direction==6 and (x+time > 7 or y+time > 7):
                return tmparray
        elif direction==7 and (x+time > 7 or y-time < 0):
                return tmparray

	#再帰させてひっくり返るか確認　ひっくり返るならreWriteを使ってひっくり返す
	elif direction==0:
		#print(direction)
                if tmparray[x-time][y] == '':
                	return tmparray
                elif num!=tmparray[x-time][y]:
                        return basic.samej(tmparray,x,y,time+1,0,num)
                else:
                        return basic.reWrite(tmparray,x,y,0,time,num)
			#print(direction+"re")                        
        elif direction==1:
                #print(direction)
                if tmparray[x+time][y] == '':
                        return tmparray
                elif num!=tmparray[x+time][y]:
                        return basic.samej(tmparray,x,y,time+1,1,num)
                else:
                        return basic.reWrite(tmparray,x,y,1,time,num)
                        #print(direction+"re")
        elif direction==2:
                #print(direction)
                if tmparray[x][y-time] == '':
                        return tmparray
                elif num!=tmparray[x][y-time]:
                        return basic.samej(tmparray,x,y,time+1,2,num)
                else:
                        return basic.reWrite(tmparray,x,y,2,time,num)
                        #print(direction+"re")
        elif direction==3:
                #print(direction)
                if tmparray[x][y+time] == '':
                        return tmparray
                elif num!=tmparray[x][y+time]:
                        return basic.samej(tmparray,x,y,time+1,3,num)
                else:
                        return basic.reWrite(tmparray,x,y,3,time,num)
                        #print(direction+"re")
        elif direction==4:
                #print(direction)
                if tmparray[x-time][y+time] == '':
                        return tmparray
                elif num!=tmparray[x-time][y+time]:
                        return basic.samej(tmparray,x,y,time+1,4,num)
                else:
                        return basic.reWrite(tmparray,x,y,4,time,num)
                        #print(direction+"re")
        elif direction==5:
                #print(direction)
                if tmparray[x-time][y-time] == '':
                        return tmparray
                elif num!=tmparray[x-time][y-time]:
                        return basic.samej(tmparray,x,y,time+1,5,num)
                else:
                        return basic.reWrite(tmparray,x,y,5,time,num)
                        #print(direction+"re")
        elif direction==6:
                #print(direction)
                if tmparray[x+time][y+time] == '':
                        return tmparray
                elif num!=tmparray[x+time][y+time]:
                        return basic.samej(tmparray,x,y,time+1,6,num)
                else:
                        return basic.reWrite(tmparray,x,y,6,time,num)
                        #print(direction+"re")
        elif direction==7:
                #print(direction)
                if tmparray[x+time][y-time] == '':
                        return tmparray
                elif num!=tmparray[x+time][y-time]:
                        return basic.samej(tmparray,x,y,time+1,7,num)
                else:
                        return basic.reWrite(tmparray,x,y,7,time,num)
                        #print(direction+"re")
	return tmparray




