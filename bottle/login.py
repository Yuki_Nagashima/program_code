# -*- coding:utf-8 -*-

from bottle import route, run, template
from bottle import get, post, request
import csv


def do_login():
    username = request.forms.get('username')
    password = request.forms.get('password')

    return "{username} {password}".format(username=username, password=password)


@route('/login', method='GET')  # or @get('/login')
def login():
    username = request.query.get('user')
    password = request.query.get('pass')
    csv_obj=csv.reader(open("memory.txt","r"))
    data=[ v for v in csv_obj]
    #GETで何も渡されていない時はusername,passwordに何も入れない
    username = "" if username is None else username
    password = "" if password is None else password

    return '''
	<form method="POST" action="/login">
	列:<input type=text name="x">
	行:<input type=text name="y"><br>
	<input type=submit name="submit" value="submit">
	<input type=submit name="submit" value="reset">
	<input type=submit name="submit" value="skip">
	</form>

    '''.format(rarray=data)


@post('/memory')
def print_memory():
    csv_obj=csv.reader(open("memory.txt","r"))
    data=[ v for v in csv_obj]
    print(data)
    #data_c = [[int(elm) for elm in v] for v in data] 
    #print(data_c)
    return template('result.php',data=data)

@route('/login', method='POST')  # or @post('/post')
def do_login():
    username = request.forms.get('username')
    password = request.forms.get('password')

    return "{username} {password}".format(username=username, password=password)


run(host='0.0.0.0', port=8080, debug=True)
