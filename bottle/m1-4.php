<?php
        header("Content-type: text/html;charset=utf-8");

	#memorydef.txtの内容をファイルに上書き
	function Resetmemory($file){
        $def='memorydef.txt';
	$defarray=Openread($def);
	Writefile($file,$defarray);
	}

	#ファイルを読み込んで配列を返す
	function Openread($file){
	$retmp=fopen($file,"r");
        while ($arList = fgetcsv($retmp)) {
                $tmparray[] = $arList;
        }
        fclose($retmp);
	return $tmparray;
	}

	#配列をファイルに書き込み
	function Writefile($file,$tmparray){
		$retmp=fopen($file,"w");
                foreach($tmparray as $row){
                        $i=0;
                        foreach($row as $tmp){
                                fwrite($retmp,"$tmp");
                                if($i<7){
                                        fwrite($retmp,",");
                                        $i++;
                                }
                        }
                fwrite($retmp,"\n");
        	}
        	fclose($retmp);
	}

	#引数に従って配列の中身を上書き
	#引数は書き換える配列、x、y、上0 下1 左2 右3 右上4 左上5 右下6 左下7、書き換える要素数、書き換える数字
	function reWrite($array,$x,$y,$direction,$num,$new){
		$tmparray=$array;
		if($direction==0){
			for($i=1;$i<$num+1;$i++){
				$tmparray[$x-$i][$y]=$new;
			}
		}
                if($direction==1){
                        for($i=1;$i<$num+1;$i++){
                                $tmparray[$x+$i][$y]=$new;
                        }
                }
                if($direction==2){
                        for($i=1;$i<$num+1;$i++){
                                $tmparray[$x][$y-$i]=$new;
                        }
                }
                if($direction==3){
                        for($i=1;$i<$num+1;$i++){
                                $tmparray[$x][$y+$i]=$new;
                        }
                }
                if($direction==4){
                        for($i=1;$i<$num+1;$i++){
                                $tmparray[$x-$i][$y+$i]=$new;
                        }
                }
                if($direction==5){
                        for($i=1;$i<$num+1;$i++){
                                $tmparray[$x-$i][$y-$i]=$new;
                        }
                }
                if($direction==6){
                        for($i=1;$i<$num+1;$i++){
                                $tmparray[$x+$i][$y+$i]=$new;
                        }
                }
                if($direction==7){
                        for($i=1;$i<$num+1;$i++){
                                $tmparray[$x+$i][$y-$i]=$new;
                        }
                }	
		return $tmparray;
	}

	#オセロみたいに挟まれたら反転する関数
	#引数　書き換える配列、x、y、繰り返し回数、上0 下1 左2 右3 右上4 左上5 右下6 左下7、0or1
	function samej($array,$x,$y,$time,$direction,$num){
		$tmparray=$array;
		if($time==0){
			$tmparray=samej($tmparray,$x,$y,$time+1,0,$num);
                        $tmparray=samej($tmparray,$x,$y,$time+1,1,$num);
                        $tmparray=samej($tmparray,$x,$y,$time+1,2,$num);
                        $tmparray=samej($tmparray,$x,$y,$time+1,3,$num);                        
			$tmparray=samej($tmparray,$x,$y,$time+1,4,$num);
                        $tmparray=samej($tmparray,$x,$y,$time+1,5,$num);
                        $tmparray=samej($tmparray,$x,$y,$time+1,6,$num);
                        $tmparray=samej($tmparray,$x,$y,$time+1,7,$num);
		}else if(($direction==0&&$x-$time<0) || ($direction==1&&$x+$time>7) || ($direction==2&&$y-$time<0) || ($direction==3&&$y+$time>7)){
			return $tmparray;
		}else if($direction==4&&($x-$time<0||$y+$time>7)){
			return $tmparray;
                }else if($direction==5&&($x-$time<0||$y-$time<0)){
                        return $tmparray;
                }else if($direction==6&&($x+$time>7||$y+$time>7)){
                        return $tmparray;
                }else if($direction==7&&($x+$time>7||$y-$time<0)){
                        return $tmparray;
		}else if($direction==0){
			if($tmparray[$x-$time][$y]!='0' && empty($tmparray[$x-$time][$y])){
				return $tmparray;
			#}else if($tmparray[$x][$y]!=$tmparray[$x-$time][$y]){
                        }else if($num!=$tmparray[$x-$time][$y]){
				return samej($tmparray,$x,$y,$time+1,0,$num);
			}else{
				#reWrite($tmparray,$x,$y,0,$time,$num);
				return reWrite($tmparray,$x,$y,0,$time,$num);
			}
		}else if($direction==1){
                        if($tmparray[$x+$time][$y]!='0' && empty($tmparray[$x+$time][$y])){
echo "e";
                                return $tmparray;
                        #}else if($tmparray[$x][$y]!=$tmparray[$x+$time][$y]){
			}else if($num!=$tmparray[$x+$time][$y]){
echo "n";
                                return samej($tmparray,$x,$y,$time+1,1,$num);
                        }else{
                                #reWrite($tmparray,$x,$y,1,$time,$num);
echo "w";
                                return reWrite($tmparray,$x,$y,1,$time,$num);
                        }
                }else if($direction==2){
                        if($tmparray[$x][$y-$time]!='0' && empty($tmparray[$x][$y-$time])){
                                return $tmparray;
                        #}else if($tmparray[$x][$y]!=$tmparray[$x][$y-$time]){
                        }else if($num!=$tmparray[$x][$y-$time]){
                                return samej($tmparray,$x,$y,$time+1,2,$num);
                        }else{
                                #reWrite($tmparray,$x,$y,2,$time,$num);
                                return reWrite($tmparray,$x,$y,2,$time,$num);
                        }
                }else if($direction==3){
                        if($tmparray[$x][$y+$time]!='0' && empty($tmparray[$x][$y+$time])){
                                return $tmparray;
                        #}else if($tmparray[$x][$y]!=$tmparray[$x][$y+$time]){
                        }else if($num!=$tmparray[$x][$y+$time]){
                                return samej($tmparray,$x,$y,$time+1,3,$num);
                        }else{
                                #reWrite($tmparray,$x,$y,3,$time,$num);
                                return reWrite($tmparray,$x,$y,3,$time,$num);
                        }
                }else if($direction==4){
                        if($tmparray[$x-$time][$y+$time]!='0' && empty($tmparray[$x-$time][$y+$time])){
                                return $tmparray;
                        #}else if($tmparray[$x][$y]!=$tmparray[$x][$y+$time]){
                        }else if($num!=$tmparray[$x-$time][$y+$time]){
                                return samej($tmparray,$x,$y,$time+1,4,$num);
                        }else{
                                #reWrite($tmparray,$x,$y,3,$time,$num);
                                return reWrite($tmparray,$x,$y,4,$time,$num);
                        }
                }else if($direction==5){
                        if($tmparray[$x-$time][$y-$time]!='0' && empty($tmparray[$x-$time][$y-$time])){
                                return $tmparray;
                        #}else if($tmparray[$x][$y]!=$tmparray[$x][$y+$time]){
                        }else if($num!=$tmparray[$x-$time][$y-$time]){
                                return samej($tmparray,$x,$y,$time+1,5,$num);
                        }else{
                                #reWrite($tmparray,$x,$y,3,$time,$num);
                                return reWrite($tmparray,$x,$y,5,$time,$num);
                        }
                }else if($direction==6){
                        if($tmparray[$x+$time][$y+$time]!='0' && empty($tmparray[$x+$time][$y+$time])){
                                return $tmparray;
                        #}else if($tmparray[$x][$y]!=$tmparray[$x][$y+$time]){
                        }else if($num!=$tmparray[$x+$time][$y+$time]){
                                return samej($tmparray,$x,$y,$time+1,6,$num);
                        }else{
                                #reWrite($tmparray,$x,$y,3,$time,$num);
                                return reWrite($tmparray,$x,$y,6,$time,$num);
                        }
                }else if($direction==7){
                        if($tmparray[$x+$time][$y-$time]!='0' && empty($tmparray[$x+$time][$y-$time])){
                                return $tmparray;
                        #}else if($tmparray[$x][$y]!=$tmparray[$x][$y+$time]){
                        }else if($num!=$tmparray[$x+$time][$y-$time]){
                                return samej($tmparray,$x,$y,$time+1,7,$num);
                        }else{
                                #reWrite($tmparray,$x,$y,3,$time,$num);
                                return reWrite($tmparray,$x,$y,7,$time,$num);
                        }
                }

		return $tmparray;

	}



?>
<html>
<head>
<Meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<form method="POST" action="m1-4.php">
列:<input type=text name="x">
行:<input type=text name="y"><br>
<input type=submit name="submit" value="submit">
<input type=submit name="submit" value="reset">
<input type=submit name="submit" value="skip">
</form>
<?php
       $next_turn=1;
       print "<input type=hidden name=\"turn\" value=$next_turn\n>";

          #ここにプログラムを書く
        $data = 'memory.txt';
        $readpoint= $data;
echo $_POST['submit'];
echo $_POST['x'];
echo $_POST['y'];

        if(!empty($_POST['submit']) && $_POST['submit']=='skip'){
                $predata=fopen('predata.txt',"r");
                $pre=intval(fread($predata,1));
                $predata=fopen('predata.txt',"w");
                if($pre==0){
                        fwrite($predata,"1");
                }else{
                        fwrite($predata,"0");
                }
                fclose($predata);
        }else if(!empty($_POST['submit']) && $_POST['submit']=='reset'){
	Resetmemory($data);
	}else if(($_POST['x']=='0' || !empty($_POST['x'])) && ($_POST['y']=='0'||!empty($_POST['y']))){
		$x=$_POST['x'];
		$y=$_POST['y'];
		$array=Openread($data);
	
		if($array[$x][$y]=='1' || $array[$x][$y]=='0'){
			echo "同じ場所にはおけません\n";
		}else{
			#配列に0と1を交互に入れる	
			$predata=fopen('predata.txt',"r");
			$pre=intval(fread($predata,1));
			#$array[$x][$y]=$pre;
			$predata=fopen('predata.txt',"w");
			if($pre==0){
				fwrite($predata,"1");
			}else{
                	        fwrite($predata,"0");
			}
			fclose($predata);
	
			#上下左右をオセロのようにひっくり返す
			$tmparray=samej($array,$x,$y,0,0,$pre);
			if($tmparray==$array){
				echo "反転しない場所にはおけません\n";
                        	$predata=fopen('predata.txt',"r");
                        	$pre=intval(fread($predata,1));
                        	#$array[$x][$y]=$pre;
                        	$predata=fopen('predata.txt',"w");
                        	if($pre==0){
                        	        fwrite($predata,"1");
                        	}else{
                        	        fwrite($predata,"0");
                        	}
                        	fclose($predata);
			}else{
				$array=$tmparray;
				$array[$x][$y]=$pre;
			}
			Writefile($data,$array);
		}
	}	
        $predata=fopen('predata.txt',"r");
        $pre=intval(fread($predata,1));
	echo "現在の手番：".$pre;
        fclose($predata);
	#処理したデータをテーブルに表示
	$rarray=Openread($data);
	echo "<table border=1>";
	foreach($rarray as $row){
	 	echo  "<tr>";
		foreach($row as $tmp){
			if($tmp==''){
				echo "<td>&nbsp&nbsp </td>";
			}else if($tmp==0){
                                echo "<td>〇</td>";
			}else if($tmp==1){
                                echo "<td>●</td>";
			}
			#echo "<td>$tmp</td>";
		}
        	echo "</tr>\n";
	}	
	echo "</table>\n";


?>
</html>
