# -*- coding:utf-8 -*-

from bottle import route, run, template
from bottle import get, post, request
import csv
import basic

#printはコンソール画面にのみ表示されるため、デバックに使用したものです
#othello.py:メインになるファイル、これ
#basic.py:othello.pyでよく使われる関数などが入っているファイル
#result.tpl:ブラウザが読み込むためにhtml表示をするためのファイル

@get('/othello')
def first_memory():
    error='' #error:未入力時等に表示するエラー文章を格納

    #predata.txtにある次の手番が白黒どちらなのか読み込み
    nextturnf = open("predata.txt","r")
    nextturn = nextturnf.read()
    #print(nextturn)
    nextturnf.close()

    #memory.txtにある現在の盤面の状態をdataに挿入
    data=basic.readfile("memory.txt")
    #print(error)

    #htmlが書かれているresult.tplにdata（盤面）、nextturn（手番）、error（エラー表示）を渡す
    return template('result',data=data,nextturn=nextturn,error=error)

    #return template('result',data=data,nextturn=nextturn)
@post('/othello')
def print_memory():
    #POSTで受け取ったデータを変数に格納
    x = request.forms.get('x')
    y = request.forms.get('y')
    method = request.forms.get('submit')
    error='' #error:未入力時等に表示するエラー文章を格納

    #リセットとスキップボタンが押された場合はそれぞれの処理
    #リセット:初期盤面　スキップ：手番飛ばし
    if method == "reset":
	reset=basic.readfile("memorydef.txt")
	basic.writefile(reset,"memory.txt")
    elif method == "skip":
	basic.skip()

    #座標の入力をせずにsubmitを押した場合のエラー
    elif x == "" or y == "":
	error = "入力してください"
	#print(error)

    #座標が入力されていた場合の処理
    elif x != [] and y != []:
	#どちらの手番かチェック
    	nextturnf = open("predata.txt","r")
    	nextturn = nextturnf.read()
    	#print(nextturn)
    	nextturnf.close()

	#現在の盤面をeditdataとtmpに挿入
	#tmp=editdataだと後の処理に不具合が生じるのでわざわざ2回読み込む（もっと良い方法あるかも）
        editdata=basic.readfile("memory.txt")
	tmp=basic.readfile("memory.txt")
	#print(tmp)
	#print(editdata)

	#editdataとtmpを使って入力された座標の位置に駒を置いた場合に
	#ひっくり返る駒が存在するか確認し、存在すればmemory.txtに上書きし、手番を入れ替える
	#なければエラー表示
	editdata=basic.samej(editdata,int(x),int(y),0,0,nextturn)
        #print(tmp)
        #print(editdata)
	if editdata == tmp:
		error="ひっくり返さない場所にはおけません"
		#print(error)
	else:
		editdata=basic.add(int(x),int(y),editdata,nextturn)
		basic.skip()
		basic.writefile(editdata,"memory.txt")


    #どちらの手番か表示するために読み込み
    nextturnf = open("predata.txt","r")
    nextturn = nextturnf.read()
    #print(nextturn)
    nextturnf.close()

    #盤面表示のためmemory.txtの内容をdataに挿入
    data=basic.readfile("memory.txt")
    #print(data)
    #data = basic.reWrite(data,int(x),int(y),0,2,nextturn)

    #htmlが書かれているresult.tplにdata（盤面）、nextturn（手番）、error（エラー表示）を渡す
    return template('result',data=data,nextturn=nextturn,error=error)






run(host='0.0.0.0', port=8080, debug=True)
