# -*- coding:utf-8 -*-
import socket
import sys

host = "127.0.0.1" #お使いのサーバーのホスト名を入れます
#host = "210.150.72.56" #お使いのサーバーのホスト名を入れます
port = int(sys.argv[1]) #クライアントと同じPORTをしてあげます

serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serversock.bind((host,port)) #IPとPORTを指定してバインドします
serversock.listen(10) #接続の待ち受けをします（キューの最大数を指定）

print 'Waiting for connections...'
clientsock, client_address = serversock.accept() #接続されればデータを格納
print(client_address[0])
#if client_address[0] != '210.150.72.56':
#    clientsock.close() 

print 'Wait...'
rcvmsg=clientsock.recv(1024)
print 'Received -> %s' % (rcvmsg)

clientsock.close()
