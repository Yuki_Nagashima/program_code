# -*- coding:utf-8 -*-
import socket
import sys
import pickle
import smtplib
from email.mime.text import MIMEText

host = "127.0.0.1" #お使いのサーバーのホスト名を入れます
#host = "210.150.72.56" #お使いのサーバーのホスト名を入れます
port = int(sys.argv[1]) #クライアントと同じPORTをしてあげます

serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serversock.bind((host,port)) #IPとPORTを指定してバインドします
serversock.listen(10) #接続の待ち受けをします（キューの最大数を指定）

print 'Waiting for connections...'
clientsock, client_address = serversock.accept() #接続されればデータを格納
print(client_address[0])
#if client_address[0] != '210.150.72.56':
#    clientsock.close() 

clientsock.sendall('宛先を入力してください') #メッセージを返します
print 'Wait...'
To=clientsock.recv(1024)
print 'Received -> To: %s' % (To)
clientsock.sendall('送信元を入力してください') #メッセージを返します
print 'Wait...'
From=clientsock.recv(1024)
print 'Received -> From: %s' % (From)
clientsock.sendall('タイトルを入力してください') #メッセージを返します
print 'Wait...'
Subject=clientsock.recv(1024)
print 'Received -> From: %s' % (Subject)
clientsock.sendall('本文を入力してください') #メッセージを返します
print 'Wait...'
mailText=pickle.loads(clientsock.recv(1024))
print 'Received -> Text: %s' % (mailText)

msg = MIMEText( mailText.encode(utf-8),'plain','utf-8' )
msg['Subject'] = Subject
msg['From']    = From
msg['To']      = To

s = smtplib.SMTP()
s.connect()
s.sendmail( me, [you], msg.as_string() )
s.close()

clientsock.sendall('メールを送信しました') #メッセージを返します

clientsock.close()
