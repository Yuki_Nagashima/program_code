# -*- coding:utf-8 -*-
import socket
import sys
import pickle
host = "127.0.0.1" #お使いのサーバーのホスト名を入れます
port = 5000 #適当なPORTを指定してあげます

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #オブジェクトの作成をします
data=pickle.dumps(sys.argv)
client.connect((host, port)) #これでサーバーに接続します
client.send(data) #適当なデータを送信します（届く側にわかるように）
#client.send(sys.argv[2])
response = client.recv(4096) #レシーブは適当な2の累乗にします（大きすぎるとダメ）

print response
